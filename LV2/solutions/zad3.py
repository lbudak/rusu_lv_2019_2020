import numpy as np
import matplotlib.pyplot as plt

spol = np.random.randint(2, size = 69420)
visine = [0] * 69420
muski = []
zenski = []

for i in range(0,69420):
    if (spol[i] == 1):
        visine[i] = np.random.normal(180,7,None)
        muski.append(visine[i])
    else:
        visine[i] = np.random.normal(167,7,None)
        zenski.append(visine[i])

plt.hist([muski,zenski],color=['blue','red'])
plt.legend(["Muski","Zemsko"])

plt.axvline(np.average(muski),color="green",linewidth=9)
plt.axvline(np.average(zenski),color="pink",linewidth=9)

plt.show()