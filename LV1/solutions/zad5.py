import numpy as np

brojevi  = []

fname = input('Enter the file name: ')  #e.g. www.py4inf.com/code/romeo.txt
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

for line in fhand:
    line = line.rstrip()
    if line.startswith("X-DSPAM-Confidence:"):
        brojevi.append(float(line.replace("X-DSPAM-Confidence: " , "")))

print("srednja vrijednost: " , np.average(brojevi))