import numpy as np

brojevi = []

while True:
    x = input("Unesite broj: ")
    if x == "Done":
        break
    try:
        x = float(x)
    except ValueError:
        print("Krivi unos")
        continue
    brojevi.append(x)

print("ima ih " , len(brojevi))
print("srednja vrijednost: " , np.average(brojevi))
print("min: " , np.min(brojevi))
print("max: " , np.max(brojevi))