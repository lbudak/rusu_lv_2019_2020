while True:
	try:
		ocjena = float(input("Unesi ocjenu: "))
		break
	except ValueError:
		print("Krivi unos")
        
if ocjena < 0.6:
	print("F")
elif ocjena >=0.6 and ocjena <0.7:
    print("D")
elif ocjena >=0.7 and ocjena <0.8:
    print("C")
elif ocjena >=0.8 and ocjena <0.9:
    print("B")
elif ocjena >=0.9:
    print("A")
