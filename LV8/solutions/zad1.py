import numpy as np
from sklearn.datasets import fetch_openml
import joblib
import sklearn.neural_network as nn
import sklearn.metrics as sm
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
import pickle


mnist = fetch_openml('mnist_784', version=1, cache=True)
X, y = mnist.data, mnist.target

#print('Got MNIST with %d training- and %d test samples' % (len(X), len(y_test)))
#print('Image size is:')

# rescale the data, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]


# TODO: build youw own neural network using sckitlearn MPLClassifier 
mlp_mnist=nn.MLPClassifier(hidden_layer_sizes=(100,100),max_iter=100,verbose=True)
mlp_mnist.fit(X_train,y_train)
mlp_mnist_predict=mlp_mnist.predict(X_train)
mlp_mnist_predict_test=mlp_mnist.predict(X_test)

# TODO: evaluate trained NN
print("Tocnost trening\n"+classification_report(y_train,mlp_mnist_predict))
print("Tocnost testno\n"+classification_report(y_test,mlp_mnist_predict_test))
plot_confusion_matrix(mlp_mnist,X_test,y_test)
plt.show()


# save NN to disk
filename = "./NN_model.sav"
joblib.dump(mlp_mnist, filename)

