from sklearn import datasets
from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plot

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, centers=4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(500, 3)
plot.scatter(data[:, 0], data[:, 1])
plot.show()
costs = []
#cluster.KMeans()

for k in range (1, 21):
 
    
    kmeans_model = cluster.KMeans(n_clusters=k, random_state=1).fit(data)
	
    
    labels = kmeans_model.labels_
 
    # Sum of distances of samples to their closest cluster center (ilitiga ono sto mi treba)
    inertia = kmeans_model.inertia_
    costs.append(inertia)
    

#kmeans = cluster.KMeans(n_clusters=4).fit(data)
#labels = kmeans.labels_

plot.plot(range(1, 21), costs)

#plot.scatter(data[:, 0], data[:, 1], c=labels)
#plot.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], c = 'black', marker = 'P')