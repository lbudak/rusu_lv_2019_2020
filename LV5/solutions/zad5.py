import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt


try:
 face = sp.face(gray=True)
except AttributeError:
 from scipy import misc
 face = misc.face(gray=True)

imageNew = mpimg.imread('../resources/example.png') 

X = imageNew.reshape((-1, 1, 3)) # We need an (n_sample, n_feature) array
k_means0 = cluster.KMeans(n_clusters=5,n_init=1).fit(X[:, :, 0])
k_means1 = cluster.KMeans(n_clusters=5,n_init=1).fit(X[:, :, 1])
k_means2 = cluster.KMeans(n_clusters=5,n_init=1).fit(X[:, :, 2])

values0 = k_means0.cluster_centers_.squeeze()
labels0 = k_means0.labels_
imageNew_compressed0 = np.choose(labels0, values0)
imageNew_compressed0.shape = imageNew.shape[0:2]

values1 = k_means1.cluster_centers_.squeeze()
labels1 = k_means1.labels_
imageNew_compressed1 = np.choose(labels1, values1)
imageNew_compressed1.shape = imageNew.shape[0:2]

values2 = k_means2.cluster_centers_.squeeze()
labels2 = k_means2.labels_
imageNew_compressed2 = np.choose(labels2, values2)
imageNew_compressed2.shape = imageNew.shape[0:2]


imageNew_compressed = np.dstack([imageNew_compressed0, imageNew_compressed1, imageNew_compressed2])

plt.figure(1)
plt.imshow(imageNew)
plt.figure(2)
plt.imshow(imageNew_compressed) 
