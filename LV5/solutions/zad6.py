import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt


def kmeans(data, k):
    x = np.random.rand(k)
    minx = np.min(data[0])
    maxx = np.max(data[0])
    x[:] = minx + x * (maxx - minx)
    y = np.random.rand(k)
    miny = np.min(data[1])
    maxy = np.max(data[1])
    y[:] = miny + y * (maxy - miny)
    
    centers = np.dstack((x, y))[0]
    plt.scatter(centers[:, 0], centers[:, 1])


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, centers=4, 
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(24, 5)

kmeans(data, 4)
