import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt


try:
 face = sp.face(gray=True)
except AttributeError:
 from scipy import misc
 face = misc.face(gray=True)

imageNew = mpimg.imread('../resources/example_grayscale.png') 

X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=5,n_init=1)
k_means.fit(X)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
imageNew_compressed = np.choose(labels, values)
imageNew_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew, cmap='gray')
plt.figure(2)
plt.imshow(imageNew_compressed, cmap='gray') 

size1 = (imageNew.shape[0]*imageNew.shape[1])/1024
size2 = size1/5
print("Original: ", size1, "Kompresirana: ", size2)
