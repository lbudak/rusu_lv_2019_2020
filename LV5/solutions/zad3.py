from sklearn import datasets
from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plot
from scipy.cluster.hierarchy import dendrogram, linkage

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, centers=4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(24, 3)
plot.scatter(data[:, 0], data[:, 1])
plot.show()

#This is also known as the Nearest Point Algorithm.
plot.subplot(7, 1, 1)
plot.title("single")
dendrogram(linkage(data, 'single'))


#This is also known by the Farthest Point Algorithm
plot.subplot(7, 1, 2)
plot.title("complete")
dendrogram(linkage(data, 'complete'))



#This is also called the UPGMA algorithm.
plot.subplot(7, 1, 3)
plot.title("average")
dendrogram(linkage(data, 'average'))



#also called WPGMA
plot.subplot(7, 1, 4)
plot.title("weighted")
dendrogram(linkage(data, 'weighted'))



#This is also known as the UPGMC algorithm.
plot.subplot(7, 1, 5)
plot.title("centroid")
dendrogram(linkage(data, 'centroid'))



#This is also known as the WPGMC algorithm.
plot.subplot(7, 1, 6)
plot.title("median")
dendrogram(linkage(data, 'median'))



# This is also known as the incremental algorithm.
plot.subplot(7, 1, 7)
plot.title("ward")
dendrogram(linkage(data, 'ward'))