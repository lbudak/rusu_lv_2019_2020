import numpy as np
from sklearn import neural_network
from sklearn import preprocessing
import matplotlib.pyplot as plt
import sklearn.metrics as sm
from sklearn.metrics import classification_report
import sklearn.linear_model as lm

def generate_data(n):
 
 #prva klasa
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 = int(n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 indices = np.random.permutation(n)
 data = data[indices,:]

 return data

#matrica zabune
def plot_confusion_matrix(c_matrix):

	norm_conf = []
	for i in c_matrix:
		a = 0
		tmp_arr = []
		a = sum(i, 0)
		for j in i:
			tmp_arr.append(float(j)/float(a))
		norm_conf.append(tmp_arr)
	
	fig = plt.figure()
	ax = fig.add_subplot(111)
	res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
	
	width = len(c_matrix)
	height = len(c_matrix[0])

	for x in range(width):
		for y in range(height):
			ax.annotate(str(c_matrix[x][y]), xy=(y, x),horizontalalignment='center',verticalalignment='center', color = 'green', size = 20)

	fig.colorbar(res)
	numbers = '0123456789'
	plt.xticks(range(width), numbers[:width])
	plt.yticks(range(height), numbers[:height])
	plt.ylabel('Stvarna klasa:')
	plt.title('Predvideno modelom:')
	plt.show()


def plot_decision_boundary(pred_func):
    # Set min and max values and give it some padding
    x_min, x_max = data_train[:, 0].min() - .5, data_train[:, 0].max() + .5
    y_min, y_max = data_train[:, 1].min() - .5, data_train[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Set3)
    plt.scatter(data_train[:, 0], data_train[:, 1], c=data_train[:,2], cmap=plt.cm.Set3)

np.random.seed(270)  
data_train=generate_data(800) #generiramo skup za učenje veličine 800 uzoraka
preprocessing.scale(data_train[:,:-1]) 

np.random.seed(50)  
data_test=generate_data(200) #generiramo skup za testiranje veličine 200 uzoraka 
preprocessing.scale(data_test[:,:-1])

nn=neural_network.MLPClassifier(hidden_layer_sizes=(4,4,4),verbose=True, max_iter=1500) #3 sloja, 4 neurona svaki sloj
nn.fit(data_train[:,:-1],data_train[:,2])#fitamo trening podacima
y_test_predict_nn= nn.predict(data_test[:,:-1])#procjena na temelju testnih podataka

logistic_model = lm.LogisticRegression()
logistic_model.fit(data_train[:,:-1], data_train[:,2])
y_test_predict_lm= logistic_model.predict(data_test[:,:-1])

matrica_cm1=sm.confusion_matrix(data_test[:,2],y_test_predict_nn[:])  #matrica zabune za neuronsku mrežu
matrica_cm2=sm.confusion_matrix(data_test[:,2],y_test_predict_lm[:])  #matrica zabune za logističku regresiju

#poziv funkcije za crtanje matrice zabune
plot_confusion_matrix(matrica_cm1)  #neuronska mreža
plot_confusion_matrix(matrica_cm2)  #logistička regresija

#crtanje klasifikacije testnog skupa za neuronsku mrežu (crveno loše, zeleno dobro)
plt.figure()
for i in range (0,len(data_test)):
    if(data_test[i,2]) == y_test_predict_nn[i]:
        color="green"  #ispravno klasificirani podaci
    else:
        color="red"  #pogresno klasificirani podaci
    plt.scatter(data_test[i,0],data_test[i,1],c=color)
plt.title("Neural Network")
plt.title('Klasifikacija neuronskom mrezom')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#crtanje klasifikacije testnog skupa za logističku regresiju (crveno loše, zeleno dobro)
plt.figure()
for i in range (0,len(data_test)):
    if(data_test[i,2]) == y_test_predict_lm[i]:
        color="green"  #ispravno klasificirani podaci
    else:
        color="red"  #pogresno klasificirani podaci
    plt.scatter(data_test[i,0],data_test[i,1],c=color,)
plt.title("Logistic Regression")
plt.title('Klasifikacija logistickom regresijom')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#crtanje granice odluke na trening podacima za neuronsku mrežu
plt.figure()
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    plt.scatter(data_train[i,0],data_train[i,1],c=color, zorder=2) #uzorci, zorder=2 -->da bude ispred pozadine
plot_decision_boundary(lambda x: nn.predict(x)) #poziv f-je za crtanje granice odluke
plt.title("Neural Network decision boundary")
plt.show()

#crtanje granice odluke na trening podacima za logističku regresiju
plt.figure()
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    plt.scatter(data_train[i,0],data_train[i,1],c=color, zorder=2) #uzorci, zorder=2 -->da bude ispred pozadine
plot_decision_boundary(lambda x: logistic_model.predict(x)) #poziv f-je za crtanje granice odluke
plt.title("Logistic Regression decision boundary")
plt.show()

#crtanje izlaza neuronske mreže u obliku vjerojatnosti zajedno s podacima za učenje (obojana slika)
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    ax.scatter(data_train[i,0],data_train[i,1],c=color, zorder=2) #uzorci, zorder=2 -->da bude ispred pozadine
plt.title("Neuron Network")
probs = nn.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz iz neuronske mreze')
plt.show()


print("Vrjednovanje klasifikatora %s:\n%s\n"% (nn, classification_report(data_test[:,2], y_test_predict_nn[:])))
print("Vrjednovanje klasifikatora %s:\n%s\n"% (logistic_model, classification_report(data_test[:,2], y_test_predict_lm[:])))