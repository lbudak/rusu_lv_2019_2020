import pandas as pd

filename = "C:/Users/lbudak/Desktop/rusu_lv_2019_2020/LV3/resources/mtcars.csv"

cars = pd.read_csv(filename)

print ("prvo\n" + str(cars.sort_values(by=['mpg'], ascending=False).head())) #moglo i sa tail, isao bi ascending

print ("drugo\n" + str(cars[cars.cyl == 8].sort_values(by=['mpg']).head(3)))

print ("trece\n" + str(cars[cars.cyl == 6].mpg.mean()))

print ("cetvrto\n" + str(cars[(cars.cyl == 4) & ((cars.wt*1000) >= 2000) & ((cars.wt*1000) <= 2200)].mpg.mean() )) 

print ("peto\nrucni: " + str(cars[cars.am == 1].am.count() ) + " automatski: " + str(cars[cars.am == 0].am.count() ))

print ("sesto\n" + str(cars[(cars.am == 0) & (cars.hp > 100)].am.count() ))

cars['wt_kg'] = cars.wt * 1000 * 0.45359237

print ("sedmo\n" + str(cars[['car','wt_kg']] ))