import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

filename = "C:/Users/lbudak/Desktop/rusu_lv_2019_2020/LV3/resources/mtcars.csv"
mtcars = pd.read_csv(filename)

cyl4 = mtcars[mtcars.cyl==4]
cyl6 = mtcars[mtcars.cyl==6]
cyl8 = mtcars[mtcars.cyl==8]


l1=np.arange(0,len(cyl4),1)
l2=np.arange(0,len(cyl6),1)
l3=np.arange(0,len(cyl8),1)

plt.figure()
plt.bar(l1,cyl4["mpg"],width=1,color='Blue')
plt.bar(l2 + 1,cyl6["mpg"],width=1,color='Green') #+1 da se ne prekopiraju
plt.bar(l3 + 2,cyl8["mpg"],width=1,color='Purple')#+2 da se ne prekopiraju
plt.title("Potrosnje automobila sa 4, 6 i 8 cilindara")
plt.xlabel("Broj auta")
plt.ylabel("Miles per Gallon")
plt.grid(linestyle='-',linewidth=1)
plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc='upper right')
plt.show()


wt4=[]
wt6=[]
wt8=[]

for i in cyl4["wt"]:
    wt4.append(i)

for i in cyl6["wt"]:
    wt6.append(i)

for i in cyl8["wt"]:
    wt8.append(i)

plt.figure()
plt.boxplot([wt4,wt6,wt8],widths=1,positions=[4,6,8])
plt.title("Mase automobila sa 4, 6 i 8")
plt.xlabel("cilindri")
plt.ylabel("Tezine")
plt.grid(linestyle='--',linewidth=2)
plt.legend(["Mase auta s 4 cilindra","Mase auta s 6 cilindara","Mase auta s 8 cilindara"],loc="upper right")
plt.show()





automatski = mtcars[mtcars.am==0]
rucni = mtcars[mtcars.am==1]
mpg_automatski = []
mpg_rucni = []

for i in automatski["mpg"]:
    mpg_automatski.append(i)

for i in rucni["mpg"]:
    mpg_rucni.append(i)

plt.figure()
plt.boxplot([mpg_rucni, mpg_automatski],positions=[0,2], widths=1)
plt.title("Potrosnje auta s automatskim i rucnim mjenjacem")
plt.xlabel("0 - rucni; 2 - automatski")
plt.ylabel("Miles per gallon")
plt.grid(linestyle='--',linewidth=1)
plt.show()


ubrzanja_rucni=[]
ubrzanja_automatski=[]
snaga_rucni=[]
snaga_automatski=[]

for i in automatski["qsec"]:
    ubrzanja_automatski.append(i)

for i in automatski["hp"]:
    snaga_automatski.append(i)
    
for i in rucni["qsec"]:
    ubrzanja_rucni.append(i)
    
for i in rucni["hp"]:
    snaga_rucni.append(i)

plt.figure()
plt.scatter(ubrzanja_rucni,snaga_rucni,marker=".",edgecolors="blue")
plt.scatter(ubrzanja_automatski,snaga_automatski,marker="x",edgecolors="green")
plt.title("Odnos snage i ubrzanja automobila")
plt.xlabel("Ubrzanje")
plt.ylabel("Snaga")
plt.legend(["Rucni mjenjac", "Automatski mjenjac"],loc="upper right")
plt.grid(linewidth=2,linestyle="--")
plt.show()