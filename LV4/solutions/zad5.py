import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

# make polynomial features
#degree=2
MSE_test=[]#srednja kvadratna pogreska za 
polydeg2 = PolynomialFeatures(degree=2)
xnewdeg2 = polydeg2.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnewdeg2))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnewdeg2)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnewdeg2)))+1:len(xnewdeg2)]

xtrain = xnewdeg2[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewdeg2[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModeldeg2 = lm.LinearRegression()#koristenje funkcije linearna regresije
linearModeldeg2.fit(xtrain,ytrain)#procjena parametara na temelju skupa za ucenje

ytest_p = linearModeldeg2.predict(xtest)#predvidanje koristeci linearni model
MSE_test.append(mean_squared_error(ytest, ytest_p))#dodaj u MSE test s vrijednostima srednjih kvadratnih pogresaka izmedu pravih i predvidenih vrijednosti

#degree=6
polydeg6 = PolynomialFeatures(degree=6)
xnewdeg6 = polydeg6.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnewdeg6))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnewdeg6)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnewdeg6)))+1:len(xnewdeg6)]

xtrain = xnewdeg6[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewdeg6[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModeldeg6 = lm.LinearRegression()
linearModeldeg6.fit(xtrain,ytrain)

ytest_p = linearModeldeg6.predict(xtest)
MSE_test.append(mean_squared_error(ytest, ytest_p))

#degree=12
polydeg12 = PolynomialFeatures(degree=15)
xnewdeg12 = polydeg12.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnewdeg12))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnewdeg12)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnewdeg12)))+1:len(xnewdeg12)]

xtrain = xnewdeg12[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewdeg12[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModeldeg12 = lm.LinearRegression()
linearModeldeg12.fit(xtrain,ytrain)

ytest_p = linearModeldeg12.predict(xtest)#
MSE_test.append(mean_squared_error(ytest, ytest_p))

#degree = 15
poly = PolynomialFeatures(degree=15)
xnew = poly.fit_transform(x)

np.random.seed(12)
indeksi = np.random.permutation(len(xnew))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)]

xtrain = xnew[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnew[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

ytest_p = linearModel.predict(xtest)
MSE_test.append(mean_squared_error(ytest, ytest_p))

plt.figure(1)
plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
plt.plot(xtest[:,1],ytest,'or',label='test')
plt.legend(loc = 4)

#pozadinska funkcija vs model
"""slika na kojoj je usporedba izlaza modela za tri različita 
stupnja dodatnih veličina u modelu (npr., degree 2, 6 i
15) s pozadinskom funkcijom koja je generirala podatke"""

plt.figure(2)
plt.plot(x,y_true,label='f')
plt.plot(x, linearModeldeg2.predict(xnewdeg2),'g-',label='model, deg=2')
plt.plot(x, linearModeldeg6.predict(xnewdeg6),'r-',label='model, deg=6')
plt.plot(x, linearModeldeg12.predict(xnewdeg12),'y-',label='model, deg=12')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4)
plt.show()
