from sklearn.datasets import load_boston
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

boston = load_boston()
X = boston.data
y = boston.target

indeksi = np.random.permutation(len(X))
indeksi_train = indeksi[0:int(np.floor(0.7*len(X)))]
indeksi_test = indeksi[int(np.floor(0.7*len(X)))+1:len(X)]

xtrain = X[indeksi_train]
ytrain = y[indeksi_train]

xtest = X[indeksi_test]
ytest = y[indeksi_test]

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

plt.figure(1)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)


print(X)
print(boston.feature_names)
